console.log("Hello World");
let firstName = "John";
let lastName = "Smith";
let age = 30;

console.log("First Name: " + firstName + "\nLast Name: " + lastName + "\nAge: " + age);

console.log("Hobbies:");
const hobbies = ["Biking", "Mountain Climbing", "Swimming"];
console.log(hobbies);

console.log("Work Address:");

let person = 
{
	houseNumber: 32,
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska"
}
console.log(person);


function returnFullname(firstName, age, lastName){
	// concatenation of parameters
	// John Doe Smith
	return firstName +' '+lastName + ' '+age;
	// the line after the return is ignored after the return statement "disregarded"
	console.log("hello world!");
};


let completeName = returnFullname(firstName, age, lastName);

console.log(firstName + ' ' +lastName +" is " + age+" years of age.");
// console.log(returnFullname(firstName, middleName, lastName));

let isMarried = true;
let isGoodConduct = false;

console.log("The value of isMarried: " + isMarried);




